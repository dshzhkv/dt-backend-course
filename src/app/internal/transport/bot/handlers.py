from telegram import Update, KeyboardButton, ReplyKeyboardMarkup
from telegram.ext import ContextTypes

from app.internal.services.user_service import UserService


user_service = UserService()
user_is_none_error_message = 'we don\'t know each other yet :(\n'\
                          'Use /start to get acquainted'


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = update.effective_user
    await user_service.save_user(user.id, user.first_name, user.last_name,
                                 user.username)
    greeting = user.first_name if len(user.first_name) > 0 else \
        (user.username if len(user.username) > 0 else 'stranger')
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f'Hi, {greeting}! Use:\n\n'
             f'/set_phone to save your phone to the system\n'
             f'/me to check your information')


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE):
    share_contact_button = KeyboardButton(text='Share contact',
                                          request_contact=True)
    reply_markup = ReplyKeyboardMarkup([[share_contact_button]],
                                       one_time_keyboard=True)
    await update.message.reply_text(text='You can share your contact now',
                                    reply_markup=reply_markup)


async def save_contact(update: Update, context: ContextTypes.DEFAULT_TYPE):
    phone = update.effective_message.contact.phone_number
    tg_id = update.effective_user.id
    is_phone_saved = await user_service.save_phone(tg_id, phone)
    if is_phone_saved:
        await update.message.reply_text(text='Your phone number is saved')
    else:
        await update.message.reply_text(
            text=f'We couldn\'t save your phone number, '
                 f'because {user_is_none_error_message}')


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await user_service.get_user_by_external_id(update.effective_user.id)
    if user is None:
        await update.message.reply_text(text=f'Unfortunately, '
                                             f'{user_is_none_error_message}')
    elif user.phone == '':
        await update.message.reply_text(text=f'Please, save your phone number '
                                             f'using /set_phone first')
    else:
        await update.message.reply_text(
            text=f'Username: {user.username}\n'
                 f'First name: {user.first_name}\n'
                 f'Last name: {user.last_name}\n'
                 f'Phone number: {user.phone}')

command_handlers = {'start': start, 'set_phone': set_phone, 'me': me}
