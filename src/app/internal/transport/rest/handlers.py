from django.views import View
from django.http import JsonResponse
from app.internal.services.user_service import UserService


class UserView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.user_service = UserService()

    async def get(self, *args, **kwargs):
        user_id = kwargs['id']
        user = await self.user_service.get_user_by_user_id(user_id)
        if user is None:
            return JsonResponse(data={'Error': f'User with id {user_id} '
                                               f'doesn\'t exist'})
        elif user.phone == '':
            return JsonResponse(data={'Error': f'User with id {user_id} '
                                               f'hasn\'t saved phone number'})
        else:
            return JsonResponse(data=dict(
                username=user.username,
                first_name=user.first_name,
                last_name=user.last_name,
                phone=user.phone
            ))
