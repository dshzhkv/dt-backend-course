from django.urls import path
from app.internal.transport.rest.handlers import UserView

urlpatterns = [
    path("me/<int:id>", UserView.as_view())
]
