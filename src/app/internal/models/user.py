from django.db import models


class User(models.Model):
    external_id = models.PositiveIntegerField(unique=True)
    username = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=12)

    def __str__(self):
        return f'{self.username}'

    class Meta:
        verbose_name = 'App User'
        verbose_name_plural = 'App Users'
