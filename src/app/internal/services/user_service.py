from asgiref.sync import sync_to_async
from app.internal.models.user import User


class UserService:
    def __init__(self):
        pass

    async def save_user(self,
                        tg_id: int,
                        first_name: str,
                        last_name: str,
                        username: str):
        await User.objects.aupdate_or_create(
            external_id=tg_id,
            username=username,
            first_name=first_name,
            last_name=last_name)

    async def get_user_by_external_id(self, tg_id: int):
        try:
            user = await User.objects.aget(external_id=tg_id)
            return user
        except User.DoesNotExist:
            return None

    async def get_user_by_user_id(self, user_id: int):
        try:
            user = await User.objects.aget(id=user_id)
            return user
        except User.DoesNotExist:
            return None

    async def save_phone(self, tg_id: int, phone: str):
        try:
            user = await User.objects.aget(external_id=tg_id)
            user.phone = phone
            await sync_to_async(user.save)()
            return True
        except User.DoesNotExist:
            return False
