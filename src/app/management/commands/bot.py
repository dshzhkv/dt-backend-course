from django.core.management.base import BaseCommand
from config.settings import BOT_TOKEN
from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, \
    filters
from app.internal.transport.bot.handlers import command_handlers, save_contact


class Command(BaseCommand):
    def handle(self, *args, **options):
        application = ApplicationBuilder().token(BOT_TOKEN).build()

        for name, handler in command_handlers.items():
            application.add_handler(CommandHandler(name, handler))
        application.add_handler(MessageHandler(filters.CONTACT, save_contact))

        application.run_polling()
