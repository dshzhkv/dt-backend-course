from django.contrib import admin

from .internal.models.user import User

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'external_id', 'username', 'phone')
